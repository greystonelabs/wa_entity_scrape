import sys, io, json, requests
import PyPDF2
from urllib.parse   import urlencode, quote
from tqdm           import tqdm
from bs4            import BeautifulSoup
from wand.image import Image
from PIL import Image as PI
import pytesseract
pytesseract.pytesseract.tesseract_cmd = '/usr/local/bin/tesseract'

class WA_State_Property_Scrape():

    def __init__(self):
        
        self.sos_base_url          = 'https://cfda.sos.wa.gov/api'
        self.tax_assessor_base_url = 'https://blue.kingcounty.com/Assessor/eRealProperty/default.aspx'
        self.tax_pmt_base_url      = 'https://payment.kingcounty.gov/Home'

        self.SOS_HEADERS = {
            "Host"           :"cfda.sos.wa.gov",
            "Connection"     :"keep-alive",
            "Accept"         :"application/json, text/plain, */*",
            "Origin"         :"https://ccfs.sos.wa.gov",
            "User-Agent"     :"Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0",
            "DNT"            :"1",
            "Content-Type"   :"application/x-www-form-urlencoded; charset=UTF-8",
            "Referer"        :"https://ccfs.sos.wa.gov/",
            "Accept-Encoding":"gzip, deflate, br",
            "Accept-Language":"en-US,en;q=0.9,he;q=0.8"
        }
        self.TAX_ASSESOR_HEADERS = {
            "Host"          : "blue.kingcounty.com",
            "Accept"        : "*/*",
            "User-Agent"    : "Rigor API Tester",
            "Content-Type"  : "application/x-www-form-urlencoded"
        }
        self.TAX_PMT_HEADERS = {
            "Accept"         : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-US,en;q=0.9,he;q=0.8",
            "Connection"     : "keep-alive",
            "DNT"            : "1",
            "Host"           : "payment.kingcounty.gov",
            "Referer"        : "https://payment.kingcounty.gov/",
            "User-Agent"     : "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0",
        }

        self.tax_pmt_cookie = self.get_cookie_for_tax_site()
    
    # ---- Site specific methods ----

    # Given text address, queries tax assessor for parcel
    def get_parcel_number(self, street, city='', postal_code=''):
        '''
        Given a text address, method queries the WA state tax assesor site for 
        a parcel number. If found, returns parcel number as string, else returns None
        '''
        ## Get form params
        r = requests.get(self.tax_assessor_base_url)
        html = r.text
        soup = BeautifulSoup(html, 'html.parser')

        payload = {
            "__EVENTTARGET"        : "",
            "__EVENTARGUMENT"      : "",
            "__VIEWSTATE"          : soup.form.find(id="__VIEWSTATE")['value'],
            "__VIEWSTATEGENERATOR" : "78617D2C",
            "__SCROLLPOSITIONX"    : "0",
            "__SCROLLPOSITIONY"    : "0",
            "__VIEWSTATEENCRYPTED" : "",
            "__EVENTVALIDATION"    : soup.form.find(id="__EVENTVALIDATION")['value'],

            "kingcounty_gov$global-search-text"          : "",
            "kingcounty_gov$cphContent$hf_accept"        : "0",
            "kingcounty_gov$cphContent$txtAddress"       : street,
            "kingcounty_gov$cphContent$txtCity"          : city,
            "kingcounty_gov$cphContent$txtZip"           : postal_code,
            "kingcounty_gov$cphContent$btn_SearchAddress": "Search",
            "kingcounty_gov$cphContent$txtParcelNbr"     : "",
            "kingcounty_gov$cphContent$txtPropName"      : ""
        }

        # I have no idea why the bs4 parse above isn't working... Hardcoding from browser. If method starts returning error
        # this is likely the culprit
        payload['__VIEWSTATE'] = 'N/hMrG9/ByT61rnCEgfWmz8THxdz4h8wNCwIarWP2B2HiiGutKC8WtX9i9gscct/n8ecZBBRqJgaAXcF+K9aSMso39WUXagJ+pPRiNnPgeJFQD7DplZAZrQdeLLEqvMwgsXgdvBVqJiDdyedh1b8ejk0rF+RFf2JyHtUdHTrbBcLCU9E/w1LtpjUlfgFWRjTtLdjRwqe+53imwW9Uk/vmMUEy3cg8swrWs63nr6vGnjDhUNhPNshZ1c/yqvmiBNz3VZs0G5Ipzju0x6lJt9GEiYwgKUZH19faxR5PKyNoCuPhYHCioxyRBYSeMocZx5ilRQxnFuvubDgDZMxCqMps6oQ2enI9gmSstCKxPtLS9C1wLQHlgfqnmQVcj+fgFH9Pft1xttOb/bYXlGFGWBzuduHf4xqxm8HfPk4cfWiztxaOpkjP9eGZ0EIh10G7CjQuQeM6Dz2C91xKrJ1NKHaIBCLNMPA/CPDPhzSnNya0+81yuPdGc8nSbaSCl3IgETSh6vgCn9xNC63vJoJs9B5DOk98TNW+grtCSTodxIgs6XeinoSv0sR/6Vx8UQnKUQLmlhZUA=='
        payload['__EVENTVALIDATION'] = 'mMxZMvI+laU4sc1k/HzyPuK7ib58cMJN7KFaXz4TaOwaeoJupTgPPndC32A28rJ9iVff1hhAqMAC5YbRQHNRsYwtgUE/uv1uMUjvqoR3svu28HWB8ObZaWtF8u+tfvPJAzHALl0c9jesZIM0Cr9JEu5YCL/WCCRSP1xz7tmF9yqBkM/yihMrN/KyXN2Mi4YHyprau71BIZbLJNRrof9QRuq26IQ='

        r = requests.post(self.tax_assessor_base_url, headers=self.TAX_ASSESOR_HEADERS, data=payload, allow_redirects=False)

        # On search success, site redirects to URL with parcelnumber as GET param. A 200 is similar to a 404 - it means the site has not found any results
        if r.status_code != 302:
            raise ValueError('Unable to find parcel number')
        
        parcel = r.headers['Location']
        parcel = parcel.split('=')[1]
        
        return parcel
    
    # Given text address, queries tax assessor for parcel
    def get_prop_data(self, street, city='', postal_code=''):
        ## Get form params
        r = requests.get(self.tax_assessor_base_url)
        html = r.text
        soup = BeautifulSoup(html, 'html.parser')

        payload = {
            "__EVENTTARGET"        : "",
            "__EVENTARGUMENT"      : "",
            "__VIEWSTATE"          : soup.form.find(id="__VIEWSTATE")['value'],
            "__VIEWSTATEGENERATOR" : "78617D2C",
            "__SCROLLPOSITIONX"    : "0",
            "__SCROLLPOSITIONY"    : "0",
            "__VIEWSTATEENCRYPTED" : "",
            "__EVENTVALIDATION"    : soup.form.find(id="__EVENTVALIDATION")['value'],

            "kingcounty_gov$global-search-text"          : "",
            "kingcounty_gov$cphContent$hf_accept"        : "0",
            "kingcounty_gov$cphContent$txtAddress"       : street,
            "kingcounty_gov$cphContent$txtCity"          : city,
            "kingcounty_gov$cphContent$txtZip"           : postal_code,
            "kingcounty_gov$cphContent$btn_SearchAddress": "Search",
            "kingcounty_gov$cphContent$txtParcelNbr"     : "",
            "kingcounty_gov$cphContent$txtPropName"      : ""
        }

        # I have no idea why the bs4 parse above isn't working... Hardcoding from browser. If method starts returning error
        # this is likely the culprit
        payload['__VIEWSTATE'] = 'N/hMrG9/ByT61rnCEgfWmz8THxdz4h8wNCwIarWP2B2HiiGutKC8WtX9i9gscct/n8ecZBBRqJgaAXcF+K9aSMso39WUXagJ+pPRiNnPgeJFQD7DplZAZrQdeLLEqvMwgsXgdvBVqJiDdyedh1b8ejk0rF+RFf2JyHtUdHTrbBcLCU9E/w1LtpjUlfgFWRjTtLdjRwqe+53imwW9Uk/vmMUEy3cg8swrWs63nr6vGnjDhUNhPNshZ1c/yqvmiBNz3VZs0G5Ipzju0x6lJt9GEiYwgKUZH19faxR5PKyNoCuPhYHCioxyRBYSeMocZx5ilRQxnFuvubDgDZMxCqMps6oQ2enI9gmSstCKxPtLS9C1wLQHlgfqnmQVcj+fgFH9Pft1xttOb/bYXlGFGWBzuduHf4xqxm8HfPk4cfWiztxaOpkjP9eGZ0EIh10G7CjQuQeM6Dz2C91xKrJ1NKHaIBCLNMPA/CPDPhzSnNya0+81yuPdGc8nSbaSCl3IgETSh6vgCn9xNC63vJoJs9B5DOk98TNW+grtCSTodxIgs6XeinoSv0sR/6Vx8UQnKUQLmlhZUA=='
        payload['__EVENTVALIDATION'] = 'mMxZMvI+laU4sc1k/HzyPuK7ib58cMJN7KFaXz4TaOwaeoJupTgPPndC32A28rJ9iVff1hhAqMAC5YbRQHNRsYwtgUE/uv1uMUjvqoR3svu28HWB8ObZaWtF8u+tfvPJAzHALl0c9jesZIM0Cr9JEu5YCL/WCCRSP1xz7tmF9yqBkM/yihMrN/KyXN2Mi4YHyprau71BIZbLJNRrof9QRuq26IQ='

        r = requests.post(self.tax_assessor_base_url, headers=self.TAX_ASSESOR_HEADERS, data=payload, allow_redirects=False)

        # On search success, site redirects to URL with parcelnumber as GET param. A 200 is similar to a 404 - it means the site has not found any results
        if r.status_code != 302:
            raise ValueError('Unable to find parcel number')
            
        prop_data={}
        url='https://blue.kingcounty.com'+r.headers['Location']
        res= requests.get(url)
        html=res.content
        re = BeautifulSoup(html, 'html.parser')
        prop_data={}
        

        rows1 = (re.find_all("table",{"class":"GridViewStyle"})[1]).find_all('tr')
        
        for row in rows1:
            aux = row.findAll('td')
            prop_data[aux[0].string] = aux[1].string
            
            
            
        rows2 = (re.find_all("table",{"class":"GridViewStyle"})[4]).find_all('tr')
        for row in rows2:
            aux = row.findAll('td')
            if len(aux)>2:
                prop_data["Valued Year"+'-'+(aux[0].string).replace(',', '')] =(aux[0].string).replace(',', '')+";"+(aux[1].string).replace(',', '')+";"+(aux[2].string).replace(',', '')+";"+(aux[3].string).replace(',', '')+";"\
                +(aux[4].string).replace(',', '')+";"+(aux[5].string).replace(',', '')+";"+(aux[6].string).replace(',', '')+";"+(aux[7].string).replace(',', '')   
                    

              
        return prop_data    

    
    # Retrieves cookie for tax payment site
    def get_cookie_for_tax_site(self):
        '''
        Method obtains proper cookie for King county (Seattle)
        tax assessor website. This is required to search & retrieve
        tax parcel information
        '''

        get_cookie_url = self.tax_pmt_base_url + '/Index?app=PropertyTaxes'

        r = requests.get(get_cookie_url, headers=self.TAX_PMT_HEADERS)

        if r.status_code != 200 or r.cookies is None:
            raise ValueError('Unable to attain Cookie from tax payment site')

        return r.cookies
    # Given parcel, queries tax payment site for taxpaying entity
    def get_entity_details_from_parcel(self, parcel):
        '''
        Given a parcel, method searches King county (Seattle) tax assessor
        site and returns Entity name and status of tax payer
        '''

        # With cookie, let's search & retrieve entity information
        tax_assessor_base_url = self.tax_pmt_base_url + '/TenantCall?app=PropertyTaxes'

        headers = {
            "Accept"         : "application/json, text/plain, */*",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-US,en;q=0.9,he;q=0.8",
            "Connection"     : "keep-alive",
            "Content-Type"   : "application/json",
            "DNT"            : "1",
            "Host"           : "payment.kingcounty.gov",
            "Origin"         : "https://payment.kingcounty.gov",
            "Referer"        : "https://payment.kingcounty.gov/Home/Index?app=PropertyTaxes",
            "User-Agent"     : "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"
        }

        payload = {
            "path"        : "RealProperty/"+parcel,
            "captchatoken": ""
        }

        parcel_tax_details = requests.post(tax_assessor_base_url, headers=headers, cookies=self.tax_pmt_cookie, data=json.dumps(payload))
        
        # Their stuff is... erm.. double encoded?

        try: 
            parcel_tax_details = json.loads(json.loads(parcel_tax_details.text)).get('data')

            entity_details = {
                "TA_Entity_Name"   : parcel_tax_details['accountName'].strip(),
                "TA_Account_Status": parcel_tax_details['accountStatus'],
                "TA_Address"       : parcel_tax_details['address1'].strip() + parcel_tax_details['address2'].strip(),
                "TA_Mortgage_Name" : parcel_tax_details['mortgageName'],
                "TA_Parcel"        : parcel
            }
        except Exception as e:
            raise ValueError(f'Error getting entity details: {str(e)}')

        return entity_details
    # Given entity name, searches SoS site
    def get_sos_search_results(self, entity_name, num_results=1000):
        '''
        Method searchs entity name on sos.wa.gov.
        Returns list of {Business_ID, Business_Name}
        Business_ID can be used to get additional information and docs
        '''
        sos_search_url = self.sos_base_url + '/BusinessSearch/GetAdvanceBusinessSearchList'

        payload = {
            "Type"                                                :"Agent",
            "BusinessStatusID"                                    :"0",
            "SearchEntityName"                                    :"%{}%".format(entity_name),
            "SearchType"                                          :"Contains",
            "BusinessTypeID"                                      :"0",
            "AgentName"                                           :"",
            "PrincipalName"                                       :"",
            "IsSearch"                                            :"true",
            "IsShowAdvanceSearch"                                 :"true",
            "AgentAddress[IsAddressSame]"                         :"false",
            "AgentAddress[IsValidAddress]"                        :"false",
            "AgentAddress[isUserNonCommercialRegisteredAgent]"    :"false",
            "AgentAddress[IsInvalidState]"                        :"false",
            "AgentAddress[baseEntity][FilerID]"                   :"0",
            "AgentAddress[baseEntity][UserID]"                    :"0",
            "AgentAddress[baseEntity][CreatedBy]"                 :"0",
            "AgentAddress[baseEntity][ModifiedBy]"                :"0",
            "AgentAddress[FullAddress]"                           :", WA, USA",
            "AgentAddress[ID]"                                    :"0",
            "AgentAddress[State]"                                 :"WA",
            "AgentAddress[Country]"                               :"USA",
            "PrincipalAddress[IsAddressSame]"                     :"false",
            "PrincipalAddress[IsValidAddress]"                    :"false",
            "PrincipalAddress[isUserNonCommercialRegisteredAgent]":"false",
            "PrincipalAddress[IsInvalidState]"                    :"false",
            "PrincipalAddress[baseEntity][FilerID]"               :"0",
            "PrincipalAddress[baseEntity][UserID]"                :"0",
            "PrincipalAddress[baseEntity][CreatedBy]"             :"0",
            "PrincipalAddress[baseEntity][ModifiedBy]"            :"0",
            "PrincipalAddress[FullAddress]"                       :", WA, USA",
            "PrincipalAddress[ID]"                                :"0",
            "PrincipalAddress[State]"                             :"",
            "PrincipalAddress[Country]"                           :"USA",
            "PageID"                                              :"1",
            "PageCount"                                           :num_results
        }
        # Encode spaces as %20 instead of default "+"
        payload = urlencode(payload, quote_via=quote)
        search_results = requests.post(sos_search_url, headers=self.SOS_HEADERS, data=payload)
        
        try:
            search_results = json.loads(search_results.text)

            businesses = [{'Business_ID' : entity['BusinessID'], 'Business_Name': entity['BusinessName']} for entity in search_results]
        
        except Exception as e:
            raise ValueError('Error getting SoS Search Results: {search_results.text}')

        return businesses
    # Given a SoS Business ID, queries top level Entity details
    def get_sos_entity_details(self, business_id):
        '''
        Given a business ID, this method queries for top level
        entity information and returns a parsed entity_details obj
        '''

        sos_entity_url = self.sos_base_url + '/BusinessSearch/BusinessInformation'

        params = {
            'businessID' : business_id
        }

        try:
            entity = requests.get(sos_entity_url, headers=self.SOS_HEADERS, params=params)
            entity = json.loads(entity.text)

            entity_details = {
                "SoS_Business_Name"     : entity.get('BusinessName'),
                "SoS_Business_Category" : entity.get('BusinessCategory'),
                "SoS_Business_Type"     : entity.get('BusinessType'),
                "SoS_Business_Status"   : entity.get('BusinessStatus'),
                "SoS_Agent_Name"        : entity['Agent'].get("EntityName"),
                "SoS_Agent_Title"       : entity['Agent'].get("Title"),
                "SoS_Agent_Email"       : entity['Agent'].get("EmailAddress"),
                "SoS_Agent_Phone"       : entity['Agent'].get("PhoneNumber"),
                "SoS_Agent_Address"     : entity['Agent']["MailingAddress"].get("FullAddress"),
                "SoS_Princ_Office_Email": entity["BusinessInfoPrincipalOffice"].get("EmailAddress"),
                "SoS_Principles"        : []
            }

            for principle in entity["PrincipalsList"]:
                entity_details["SoS_Principles"].append({
                    'Type'      : principle.get("TypeID"),
                    'First_Name': principle.get("FirstName"),
                    'Last_Name' : principle.get("LastName"),
                    'Title'     : principle.get("Title"),
                    'Address'   : principle["PrincipalStreetAddress"].get("FullAddress"),
                    'Phone'     : principle["PhoneNumber"],
                    'Email'     : principle["EmailAddress"]
                })

        except Exception as e:
            raise ValueError('Error getting SoS entity details: {entity.text}')
        
        return json.dumps(entity_details)
    # Given a SoS Business ID, queries documents associated to business
    def get_sos_entity_filings(self, business_id):
        '''
        Given a SoS business_id this method gets all filings
        related to the business and returns a list of filing_detail objs
        which is used by the get_document function
        '''

        # In order to get the actual document from SoS, we first have to query
        # for the doc id by sending a filing number/ID
        sos_filing_url        = self.sos_base_url + '/BusinessSearch/GetBusinessFilingList?'
        sos_doc_id_query_url  = self.sos_base_url + '/Common/GetTransactionDocumentsList'
        sos_doc_url           = self.sos_base_url + '/Common/DownloadOnlineFilesByNumber?'

        accepted_file_types = ["ANNUAL REPORT", "CERTIFICATE OF FORMATION", "INITIAL REPORT"]
        
        params = {
            'IsOnline'  : 'true',
            'businessId': business_id
        }

        # Go through available documents and filter for applicable docs
        available_filings = requests.get(sos_filing_url, headers=self.SOS_HEADERS, params=params)
        
        try:
            available_filings = json.loads(available_filings.text)
            
            filing_details = []

            for filing in available_filings:
                # filter for accepted docs
                if filing["FilingTypeName"] not in accepted_file_types:
                    continue
                else:
                    # ensures we only grab a single, latest instance of document - else we'd have 30 Annual Reports to parse
                    accepted_file_types = [type_name for type_name in accepted_file_types if type_name != filing["FilingTypeName"]]
                
                filing_data = {
                    "ID"          : str(filing["Transactionid"]),
                    "FilingNumber": str(filing["FilingNumber"])
                }

                # with filing number/id, query for actual doc id
                doc_details = requests.post(sos_doc_id_query_url, headers=self.SOS_HEADERS, data=filing_data)
                doc_details = json.loads(doc_details.text)[0]

                # Create a filing_detail obj, which is used by the get_document method, and append to filing_details
                doc_url = sos_doc_url + f'fileName={doc_details["FileLocationCorrespondence"]}' \
                                    + f'&CorrespondenceFileName={doc_details["CorrespondenceFileName"]}' \
                                    + f'&DocumentTypeId={doc_details["DocumentTypeID"]}'

                filing_details.append({
                    'url'                       : doc_url,
                    'DocumentID'                : doc_details["DocumentID"],
                    'DocumentTypeID'            : doc_details["DocumentTypeID"],
                    'FileLocationCorrespondence': doc_details["FileLocationCorrespondence"],
                    'CorrespondenceFileName'    : doc_details["CorrespondenceFileName"]
                })

        except Exception as e:
            raise ValueError('Error in retrieving SoS filings: {available_filings.text}')
             
        return filing_details
    # Given a filing_detail, downloads actual document
    def get_document(self, filing_detail):
        '''
        Given a filing detail obj (returned from get_sos_entity_filings)
        this method pulls the raw file bytes. Can be used to write PDFs
        '''

        url = filing_detail['url']

        payload = {
            "DocumentID"                : filing_detail['DocumentID'],
            "DocumentTypeID"            : filing_detail['DocumentTypeID'],
            "DocumentTypeName"          : "BUSINESS DOCUMENT",
            "FileLocationCorrespondence": filing_detail['FileLocationCorrespondence'],
            "CorrespondenceFileName"    : filing_detail['CorrespondenceFileName'],
            "FilingNumber"              : "0",
            "TransactionID"             : "0",
            "WorkorderID"               : "0",
            "BusinessID"                : "0",
            "TrademarkID"               : "0",
            "RegistrationNo"            : "0",
            "IsOnline"                  : "false",
            "FilingDateTime"            : "2005-01-05T14:09:55",
            "UserName"                  : "BATCH JOB",
            "IsPublicVisible"           : "false",
            "Source"                    : "INHOUSE",
            "NumberOfPages"             : "0",
            "UncommitedTID"             : "0",
            "CreatedDate"               : "0001-01-01T00:00:00",
            "FilerID"                   : "0",
            "UserID"                    : "0",
            "CreatedBy"                 : "0",
            "ModifiedBy"                : "0"
        }

        raw_doc = requests.post(url, headers=self.SOS_HEADERS, data=payload)
        try:
            doc     = io.BytesIO(raw_doc.content)
        
        except Exception as e:
            raise ValueError('Error getting document from SoS: {raw_doc.text}')

        return doc

    # ---- Helper Methods ----

    # Given a PDF, parses and returns text
    def parse_pdf(self, pdf_file_obj):
        '''
        Given a pdf file, this method parses the 
        file and returns a body of string
        '''

        pdfReader = PyPDF2.PdfFileReader(pdf_file_obj)
        num_pages = pdfReader.numPages
        count     = 0
        text      = ''

        while count < num_pages:
            pageObj = pdfReader.getPage(count)
            count  += 1
            text   += pageObj.extractText()
        
        return text
    
        # Given a OCR text parsing
    def ocr_parse_pdf(self, pdf_file_obj):    
        #OCR on PDF files using Python   
        # install Tesseract , PyOCR, Wand and PIL
        # imported Image from PIL as PI because otherwise it would have conflicted with the Image module from wand.image
        #setup two lists which will be used to hold our images and final_text.
        req_image = []
        text_list = []
        
        #open the PDF file using wand and convert it to jpeg  
     
        image_pdf = Image(filename=pdf_file_obj, resolution=300)
        image_jpeg = image_pdf.convert('jpeg')
        
        #wand has converted all the separate pages in the PDF into separate image blobs. We can loop over them and append them as a blob into the req_image list.
        for img in image_jpeg.sequence:
            img_page = Image(image=img)
            req_image.append(img_page.make_blob('jpeg'))
        
        # run OCR over the image blobs
        #all of the recognized text has been appended in the final_text list 
        for img in req_image: 
            pg_txt = pytesseract.image_to_string(PI.open(io.BytesIO(img)), lang='eng')
            text_list.append(pg_txt)
        
        text = ''    
        text= ''.join(str(e) for e in text_list)    
        return text  


    # given a body of text, returns email addresses
    def extract_emails(self, text):
        '''
        Given a body of text, this method looks
        for all text that contains a '@' char
        and returns a set of results
        '''

        words  = text.split(' ')
        emails = {word for word in words if '@' in word}

        return emails
    # Given an entity name, strips all pluses and other found strings that trip the SoS search
    def clean_entity_term(self, entity_term):

        # found this instance: FG92 LENORA LLC+CWS LENORA
        if '+' in entity_term:
            entity_term = entity_term.split('+')[0]

        # Remove commas, so entity, LLC matches entity LLC
        if ',' in entity_term:
            entity_term = entity_term.replace(',', '')

        return entity_term
    
    # Flatten JSON responses to delimited text
    def flatten_results(self, json_details):
        
        result_str = 'Street|TA_Entity_Name|TA_Address|TA_Parcel|prop_data|' \
                    +'SoS_Entity_Name|SoS_Business_Status|SoS_Agent_Name|' \
                    +'SoS_Agent_Title|SoS_Agent_Phone|SoS_Agent_Address|Principles|Emails \n'

        for street in json_details.keys():

            details = json_details[street]

            if details["TA_Results"].get('TA_Entity_Name', None) == None:
                continue

            TA_Entity_Name = details["TA_Results"]["TA_Entity_Name"]
            TA_Address     = details["TA_Results"]["TA_Address"]
            TA_Parcel      = details["TA_Results"]["TA_Parcel"]
                      
            prop_data = details["prop_data"]
            
            SoS_results = details["SoS_Results"]
        

            if len(SoS_results) == 0:
                result_str += f'"{street}|{TA_Entity_Name}|{TA_Address}|{TA_Parcel}|{prop_data}"'.replace('\n','')  + '\n'
            
                continue
                
            for entity in SoS_results.keys():

                emails = []

                SoS_Business_Status = SoS_results[entity]['SoS_Business_Status']
                SoS_Agent_Name      = SoS_results[entity]['SoS_Agent_Name']
                SoS_Agent_Title     = SoS_results[entity]['SoS_Agent_Title']
                SoS_Agent_Phone     = SoS_results[entity]['SoS_Agent_Phone']
                SoS_Agent_Address   = SoS_results[entity]['SoS_Agent_Address']
                if SoS_results[entity]['SoS_Agent_Email'] != 'null' and SoS_results[entity]['SoS_Agent_Email'] != None and SoS_results[entity]['SoS_Agent_Email'] != '':
                    emails.append(SoS_results[entity]['SoS_Agent_Email'])
                if 'SoS_PDF_parsed_emails' in SoS_results[entity]:
                    if SoS_results[entity]['SoS_PDF_parsed_emails'] != 'null' and SoS_results[entity]['SoS_PDF_parsed_emails'] != None and SoS_results[entity]['SoS_PDF_parsed_emails'] != '':
                        emails.extend(SoS_results[entity]["SoS_PDF_parsed_emails"])

                principles = SoS_results[entity]["SoS_Principles"]
                principle_str = ''

                for principle in principles:
                    name  = principle['First_Name'] + ' ' + principle['Last_Name']
                    title = principle['Title']
                    addr  = principle['Address']
                    phone = principle['Phone']
                    email = principle['Email']
                    if email != 'null' and email != '' and email != None:
                        emails.append(email)

                    principle_str += f'"{name}-{title}-{addr}-{phone}-{email};"'.replace('\n','') 

                email_str = ''
                if emails:
                    email_str = '-'.join(emails)

                result_str += f'"{street}|{TA_Entity_Name}|{TA_Address}|{TA_Parcel}|{prop_data}|{entity}|{SoS_Business_Status}|{SoS_Agent_Name}|{SoS_Agent_Title}|{SoS_Agent_Phone}|{SoS_Agent_Address}|{principle_str}|{email_str}"'.replace('\n','')  + '\n'
    
        
            
        return result_str
    # Write CSV to path
    def save_csv(self, csv_str, path_to_save='res.csv'):
        '''
        Writes csv_str string directly to a csv file
        on the local drive
        '''
        with open(path_to_save, "w+") as f:
            print(f"{csv_str}", file=f)
    
    # ---- Main methods that combine all of the above into single calls ----
    
    # Coordinates full SoS operations
    def search_and_extract_SoS_details(self, search_term, num_results=5):
        '''
        This is our main function
        Given a search term, this method calls all other
        class methods to search, parse and return entity info for 
        all search results. The num_results param can be used to limit how
        many search results to parse
        '''

        results = {}

        try:
            businesses = self.get_sos_search_results(search_term, num_results=num_results)
        except Exception as e:
            raise ValueError(f'{str(e)}')
        
        if len(businesses) == 0:
            return results # No results

        for business in businesses:
            b_id = business['Business_ID']

            entity_info = self.get_sos_entity_details(b_id)
            entity_info = json.loads(entity_info)

            filings = self.get_sos_entity_filings(b_id)
            if len(filings) == 0:
                entity_name = entity_info['SoS_Business_Name']
                results[entity_name] = entity_info
                continue
            
            entity_info['SoS_PDF_parsed_emails'] = set()

            for filing_detail in filings:
                doc    = self.get_document(filing_detail)
                text   = self.parse_pdf(doc)
                if len(text)==0:
                    text=self.ocr_parse_pdf(doc)
                else:
                    break 
                emails = self.extract_emails(text)
                entity_info['SoS_PDF_parsed_emails'] = entity_info['SoS_PDF_parsed_emails'] | emails
            
            # Convert set to list so that it is JSON serializable
            entity_info['SoS_PDF_parsed_emails'] = list(entity_info['SoS_PDF_parsed_emails'])
            entity_name = entity_info['SoS_Business_Name']
            results[entity_name] = entity_info
        
        return results
    # Coordinates full Property Search
    def search_and_extract_Entity_details(self, street, city='', postal_code=''):

        entity_info = None

        try:
            parcel      = self.get_parcel_number(street, city, postal_code)
            entity_info = self.get_entity_details_from_parcel(parcel)

        except Exception as e:
            raise ValueError(f'Error: {str(e)}')

        return entity_info
    
    # Coordinates full Property related search
    def search_and_extract_property_details(self, street, city='', postal_code=''):

        try:
            prop_data = self.get_prop_data(street, city, postal_code)

        except Exception as e:
            raise ValueError('prop related error: {str(e)}')

        return prop_data
    
    
    # Coordinates full Prop, Entity and SoS search
    def search_and_extract_from_prop_list(self, properties, flatten=True):
        '''
        Given a list of property objects, {street='123 main st', city='Seattle', postal_code=''}
        Return full Parcel, Entity, Contact Detail Results
        '''

        results = {}

        for property_obj in tqdm(properties):
            error       = None
            street      = property_obj['street']
            city        = property_obj['city']
            postal_code = property_obj['postal_code']
            entity_details = {}
            SoS_results    = {}
            prop_data={}

            try:
                entity_details= self.search_and_extract_Entity_details(street, city=city, postal_code=postal_code)
                entity_search_term = entity_details['TA_Entity_Name']
                entity_search_term = self.clean_entity_term(entity_search_term)

            except Exception as e:
                error = str(e)
            
            else:
                try:
                    SoS_results  = self.search_and_extract_SoS_details(entity_search_term)
                    prop_data=self.search_and_extract_property_details(street, city=city, postal_code=postal_code)
                
                except Exception as e:
                    error = str(e)
            

            finally:
                results[street] = {
                    'TA_Results' : entity_details,
                    'prop_data':  prop_data,
                    'SoS_Results': SoS_results,
                    'Error'      : error
                }

        if flatten:
            results = self.flatten_results(results)
        else:
            results = json.dumps(results)

        return results

if __name__ == "__main__":

    street      = sys.argv[1]
    city        = ''
    postal_code = ''

    if len(sys.argv) > 2:
        city = sys.argv[2]
    if len(sys.argv) > 3:
        postal_code = sys.argv[3]
    
    sos_scraper = WA_State_Property_Scrape()

    print(f'starting search for {street}, {city} {postal_code}')
    prop_obj = {
        'street'     :street,
        'city'       :city,
        'postal_code':postal_code
    }
    results = sos_scraper.search_and_extract_from_prop_list([prop_obj], flatten=False)
    print(results)