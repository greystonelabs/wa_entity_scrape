from state_property_scrape import WA_State_Property_Scrape

prop_objs = [
    {
        'street'     :'211 Lenora St',
        'city'       :'Seattle',
        'postal_code':''
    },
    {
        'street'     :'error street!',
        'city'       :'',
        'postal_code':''
    },
    {
        'street'     :'25 Pike St',
        'city'       :'Seattle',
        'postal_code':''
    }
]

scraper = WA_State_Property_Scrape()
results = scraper.search_and_extract_from_prop_list(prop_objs)

print(results)